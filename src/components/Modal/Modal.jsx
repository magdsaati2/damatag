import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

export default function AppDialog({ open, handleClose, dialogInfo, onAgree }) {
  return (
    <React.Fragment>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle
          sx={{
            color: "var(--main-color)",
            textAlign: "center",
            fontWeight: "bold",
          }}
        >
          {dialogInfo.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText sx={{ color: "#000", textAlign: "center" }}>
            {dialogInfo.desc}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button sx={ {color: "gray"}} onClick={handleClose}>Cancel</Button>
          <Button sx={ {color: "var(--main-color)"}} onClick={onAgree} autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
