import { Button, ButtonGroup } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import TaskAltIcon from "@mui/icons-material/TaskAlt";
import HourglassEmptyIcon from "@mui/icons-material/HourglassEmpty";
import AppDialog from "./Modal/Modal";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { Box, Tooltip } from "@mui/material";
import { changePostStatus } from "../state/postSlice";
const PostListItem = ({ data, deleteRecord, isLoggedIn }) => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [clickedTask, setClickedTask] = useState(false);
  const [dialogInfo, setDialogInfo] = useState({
    title: "",
    desc: "",
  });

  const handleClickOpen = (el, type) => {
    setOpen(true);
    setClickedTask(el.id);
    if (type === "pending") {
      setDialogInfo({
        title: "Task to pending",
        desc: `Are you sure you put the task "${el.title} in the completed state?`,
      });
    } else if (type === "completed") {
      setDialogInfo({
        title: "Task to complete",
        desc: `Are you sure you put the task ${el.title} in the pending state?`,
      });
    }
  };

  const handleClose = () => {
    setOpen(false);
  };
  const navigate = useNavigate();

  const deleteHandler = (item) => {
    if (window.confirm(`Do you really want to delete record: ${item.title}?`)) {
      deleteRecord(item.id);
    }
  };

  const records = data?.map((el, idx) => (
    <tr key={el.id}>
      <td>#{++idx}</td>
      <td>
        <Link to={`post/${el?.id}`}>{el?.title}</Link>
      </td>
      <td>
        <ButtonGroup aria-label="Basic example">
          <Button
            variant="success"
            onClick={() => navigate(`post/${el?.id}/edit`)}
          >
            Edit
          </Button>
          <Button
            variant="danger"
            onClick={() => deleteHandler(el)}
            disabled={!isLoggedIn}
          >
            Delete
          </Button>
        </ButtonGroup>
      </td>
      <td>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Tooltip title="change the status to pending">
            <HourglassEmptyIcon
              style={{
                color: el.type === "pending" ? "blue" : "#ccc",
                fontSize: "2rem",
                cursor: "pointer",
              }}
              onClick={() => handleClickOpen(el, "pending")}
            />
          </Tooltip>
          <Tooltip title="change the status to completed">
            <TaskAltIcon
              style={{
                color: el.type === "completed" ? "green" : "#ccc",
                fontSize: "2rem",
                cursor: "pointer",
              }}
              onClick={() => handleClickOpen(el, "completed")}
            />{" "}
          </Tooltip>
        </Box>
      </td>
    </tr>
  ));
  return (
    <>
      {records}
      <AppDialog
        open={open}
        handleClose={handleClose}
        handleClickOpen={handleClickOpen}
        dialogInfo={dialogInfo}
        onAgree={() => {
          if (dialogInfo.title === "Task to pending") {
            dispatch(changePostStatus({ id: clickedTask, status: "pending" }));
          } else if (dialogInfo.title === "Task to complete") {
            dispatch(
              changePostStatus({ id: clickedTask, status: "completed" })
            );
          }
          handleClose();
        }}
      />
    </>
  );
};

export default PostListItem;
