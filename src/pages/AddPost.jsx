import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { insertPost } from "../state/postSlice";
import withGuard from "../util/withGuard";
import { useFormik } from "formik";
import { postSchema } from "../util/validationSchema";

import { TextField, Button, Typography, Box, Grid } from "@mui/material";
import Loading from "../components/Loading";
import { ErrorMessage } from "../util/ErrorMessage";
import SendIcon from "@mui/icons-material/Send";
import addImg from "../images/addImg.jpg";
const AddPost = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { loading, error } = useSelector((state) => state.posts);

  const formik = useFormik({
    initialValues: {
      title: "",
      description: "",
    },
    validationSchema: postSchema,
    onSubmit: (values) => {
      const id = Math.floor(Math.random() * 500);
      const userInfo = JSON.parse(localStorage.getItem("info"));
      const userId = userInfo ? userInfo.userId : null;
      dispatch(
        insertPost({
          id,
          title: values.title,
          description: values.description,
          type: "",
          userId
        })
      )
        .unwrap()
        .then(() => {
          navigate("/");
        })
        .catch((error) => {
          console.log(error);
        });
    },
  });

  return (
    <Grid container spacing={2} my={2}>
      <Grid item sm={12} lg={6}>
        <Typography variant="h2" gutterBottom>
          Add New Item
        </Typography>
        <Box component="form" onSubmit={formik.handleSubmit}>
          <Typography variant="h6" gutterBottom>
            Title
          </Typography>
          <TextField
            fullWidth
            type="text"
            name="title"
            onChange={formik.handleChange}
            value={formik.values.title}
            error={!!formik.errors.title}
            helperText={<ErrorMessage field="title" formik={formik} />}
          />
          <Typography variant="h6" gutterBottom>
            Description
          </Typography>
          <TextField
            fullWidth
            multiline
            rows={3}
            name="description"
            onChange={formik.handleChange}
            value={formik.values.description}
            error={!!formik.errors.description}
            helperText={<ErrorMessage field="description" formik={formik} />}
          />
          <Loading loading={loading} error={error}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              sx={{ mt: 2 }}
              endIcon={<SendIcon />}
            >
              Submit
            </Button>
          </Loading>
        </Box>{" "}
      </Grid>
      <Grid sm={12} lg={6}>
        <img src={addImg} alt="add-img" style={{ maxWidth: "100%", height: "auto" }} />
      </Grid>
    </Grid>
  );
};

export default withGuard(AddPost);
