import { useEffect } from "react";
import { useDispatch } from "react-redux";
import usePostDetails from "../hooks/use-post-details";
import Loading from "../components/Loading";
import { Card, CardContent, Typography, Box, CardMedia } from "@mui/material";
import taskImg from "../../src/images/taskImg.jpg";
const Details = () => {
  const dispatch = useDispatch();

  const { loading, error, record } = usePostDetails();

  useEffect(() => {
    return () => {
      dispatch({ type: "posts/cleanRecord" });
    };
  }, [dispatch]);

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <Card>
        <img
          src={taskImg}
          alt="post-img"
          style={{ maxWidth: "100%", height: "auto" }}
          title="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {record?.title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {record?.description}
          </Typography>
        </CardContent>
      </Card>
    </Box>
  );
};

export default Details;
