import { useEffect } from "react";
import usePostDetails from "../hooks/use-post-details";
import { useDispatch } from "react-redux";
import { editPost } from "../state/postSlice";
import { useNavigate } from "react-router-dom";
import withGuard from "../util/withGuard";
import { useFormik } from "formik";
import { postSchema } from "../util/validationSchema";

import Loading from "../components/Loading";
import { TextField, Button, Typography, Box, Grid } from "@mui/material";
import editImg from "../images/editImg.jpg";
import ModeEditOutlineIcon from "@mui/icons-material/ModeEditOutline";
const EditPost = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { loading, error, record } = usePostDetails();

  useEffect(() => {
    return () => {
      dispatch({ type: "posts/searchRecord" });
    };
  }, [dispatch]);

  const formik = useFormik({
    initialValues: {
      title: record ? record.title : "",
      description: record ? record.description : "",
    },
    enableReinitialize: true,
    validationSchema: postSchema,
    onSubmit: (values) => {
      dispatch(
        editPost({
          id: record.id,
          title: values.title,
          description: values.description,
        })
      )
        .unwrap()
        .then(() => navigate("/"));
    },
  });

  return (
    <Grid container spacing={2} my={2}>
      <Grid item sm={12} lg={6}>
        <Typography variant="h2" gutterBottom>
          Edit Item
        </Typography>
        <Box component="form" onSubmit={formik.handleSubmit} sx={{ p: 3 }}>
          <Typography variant="h6" gutterBottom>
            Title
          </Typography>
          <TextField
            fullWidth
            type="text"
            name="title"
            value={formik.values.title}
            onChange={formik.handleChange}
            error={!!formik.errors.title}
            helperText={formik.errors.title}
          />
          <Typography variant="h6" gutterBottom>
            Description
          </Typography>
          <TextField
            fullWidth
            multiline
            rows={3}
            name="description"
            value={formik.values.description}
            onChange={formik.handleChange}
            error={!!formik.errors.description}
            helperText={formik.errors.description}
          />
          <Loading loading={loading} error={error}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              sx={{ mt: 2 }}
              endIcon={<ModeEditOutlineIcon />}
            >
              Submit
            </Button>
          </Loading>
        </Box>
      </Grid>
      <Grid item sm={12} lg={6}>
        <img
          src={editImg}
          alt="edit-img"
          style={{ maxWidth: "100%", height: "auto" }}
        />
      </Grid>
    </Grid>
  );
};

export default withGuard(EditPost);
