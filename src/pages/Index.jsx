import { useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPosts, deletePost } from "../state/postSlice";
import PostList from "../components/PostList";
import Loading from "../components/Loading";
import { Box, Typography } from "@mui/material";
import Select from "react-select";
import { useState } from "react";
import { customSelectTheme } from "../util/SelectStyle";
import nodatavector from "../images/nodatavector.jpg";
const Index = () => {
  const dispatch = useDispatch();
  const { records, loading, error } = useSelector((state) => state.posts);
  const [items, setItems] = useState([]);
  const { isLoggedIn } = useSelector((state) => state.auth);
  let filterOptions = [
    {
      label: "All",
      value: "all",
    },
    {
      label: "Completed",
      value: "completed",
    },
    {
      label: "Pending",
      value: "pending",
    },
  ];

  useEffect(() => {
    dispatch(fetchPosts());
  }, [dispatch]);

  useEffect(() => {
    const userInfo = JSON.parse(localStorage.getItem("info"));
    const userId = userInfo ? userInfo.userId : null;
    if (userId) {
      setItems(records.filter((el) => el.userId === userId));
    }
  }, [records]);

  const deleteRecord = useCallback(
    (id) => dispatch(deletePost(id)),
    [dispatch]
  );

  return (
    <Loading loading={loading} error={error}>
      <Box sx={{ padding: "2em" }}>
        {/*  filter  */}

        <Box sx={{ display: "flex", margin: "0.5em 0", gap: "0.3em" }}>
          <Typography
            variant="h6"
            noWrap
            component="div"
            sx={{
              display: { xs: "none", sm: "block" },
              color: "black",
              marginLeft: "20px",
              fontWeight: "bold",
            }}
          >
            Filter Tasks By:
          </Typography>

          <Select
            options={filterOptions}
            onChange={(selectedItem) => {
              let filteredItems;
              const userInfo = JSON.parse(localStorage.getItem("info"));
              const userId = userInfo ? userInfo.userId : null;
              if (selectedItem.value === "all") {
                filteredItems = records.filter((el) => el.userId === userId);
              } else {
                filteredItems = records
                  .filter((el) => el.userId === userId)
                  .filter((item) => {
                    return item.type === selectedItem.value;
                  });
              }
              setItems(filteredItems);
            }}
            theme={customSelectTheme}
          />
        </Box>
        {items?.length > 0 ? (
          <PostList
            data={items}
            deleteRecord={deleteRecord}
            isLoggedIn={isLoggedIn}
          />
        ) : (
          <img
            style={{
              maxWidth: "100%",
              height: "auto",
              position: "absolute",
              top: "50%",
              left: "50%",
              transform: "translate(-50%,-50%)",
            }}
            src={nodatavector}
            alt="no-data"
          />
        )}
      </Box>
    </Loading>
  );
};

export default Index;
