import { Outlet } from "react-router-dom";

import { Container, Row, Col } from "react-bootstrap";
import SearchAppBar from "../components/Header";
import { Fragment } from "react";

const RootLayout = () => {
  return (
    <Fragment>
      <SearchAppBar />
      <Container>
        <Row>
          <Col xs={{ span: 8, offset: 2 }}>
            <Outlet />
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
};

export default RootLayout;
