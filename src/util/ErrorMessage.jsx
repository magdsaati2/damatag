import { Typography } from "@mui/material";


export const ErrorMessage = ({ field, formik, color }) => {
  return (
    <>
      {formik.touched[field] && !!formik.errors[field] && (
        <Typography
          style={{
            color: "red",
            fontSize: "0.8rem",
          }}
        >
          {(formik.errors[field])}
        </Typography>
      )}
    </>
  );
};
