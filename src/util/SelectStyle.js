export const customSelectTheme = (theme) => ({
    ...theme,
    borderRadius: "5px",
    colors: {
      ...theme.colors,
      primary25: "var(--sec-color)",
      primary50: "var(--sec-color)",
      primary: "var(--sec-color)",
    },
  });