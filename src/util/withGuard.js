import { useSelector } from "react-redux";
import SignIn from "../components/Signin/SignIn";

const withGuard = (Component) => {
  const Wrapper = (props) => {
    const { isLoggedIn } = useSelector((state) => state.auth);

    return isLoggedIn ? <Component  {...props} /> : <SignIn />;
  };
  return Wrapper;
};

export default withGuard;
